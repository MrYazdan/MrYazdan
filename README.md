# ✋ Hi there, I'm Yazdan

<div align="left">
    <picture align="right">
        <img align="right" width="340em" height="340em" src="tech.gif">
    </picture>
<br>
     
🌱  &nbsp;I’m currently learning **LPIC2, SANS SEC-580 MSF**

💬  &nbsp;Ask me about **Linux 🐧** | **Python 🐍**

⚡ &nbsp;**I ❤️️ FSF + Linux + ☕** !
</div>

<br>
